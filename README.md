# CarCar:

CarCar is web based application designed to handle the inventory of automobiles, the records of automobile sales and service history, company personnel and customer history like any modern dealership should have.

# Team:

* Riley Schorzman - Sales
* Malek Byam - Services

# Startup/Run - (Requires Docker, Git, and Node.js)

Step 1. Fork this repository

Step 2. Clone the new repository into your system using:
        <git clone https://gitlab.com/malek.byam/CarCar>
        Then enter the top level directory of the project.

Step 3. Create a docker volume using:
        "docker volume create beta-data"
        Prepare images and create containers using:
        "docker-compose build"
        Run the application using:
        "docker-compose up"

Step 4. Wait for all docker containers to be running
        and for the react container to read:
        (You can now view app in the browser.)
        To view the application use http://localhost:3000/ in your browser.

## Design

This application is made up of 3 microservices that are:

1. Inventory - This microservice has three models; Manufacturer, VehicleModel with a
        foreign key to Manufacturers, and Automobile with a foreign key to VehicleModel.
        Each model has associated views to be able to list, create, update, and delete an
        associated object. In React there is page/file to create and list an object of
        each model.

2. Sales - This microservice has four models; Salesperson, Customer, AutomobileVO
        which gets select data from the Inventory Automobile objects via a poller,
        and Sale with foreign keys to Salesperson, Customer and AutomobileVO.
        Each model except AutomobileVO has associated views to be able to list, create,
        and delete an associated object. In React there is page/file to create and list
        an object of each model with views. The creation of a sale in React will also
        update an Inventory Automobile objects sold field.

3. Services - This microservice has three models; Technician, AutomobileVO
        which gets select data from the Inventory Automobile objects via a poller,
        and Automobile with a foreign key to Technician.
        Each model except AutomobileVO has associated views to be able to list, create,
        and delete an associated object as well as change the status of appointments.
        In React there is page/file to create and list an object of each model with views.

## Service microservice

    Technician
        You can view a list of technicians with the first name, last name, and employee ID.
        Technicians can also be created and after they are created can by assigned in appointments.

    Appointments
        You can view a list of appointments with the date, time, reason, status, vin, and customer name.
        When appointments are cancelled or finished they no longer appear on the appointments list.
        There is a record of all appointments in the Service history, that can be searched by VIN number.
        You can also create appointments.


## Diagram
 - PROJECT-BETA/Diagram.png

### URLs and Ports for Insomnia
-| Action -----------------| Method > URL

Inventory - Port:8100
-| List manufacturers -----| GET ---> http://localhost:8100/api/manufacturers/
-| Create a manufacturer --| POST --> http://localhost:8100/api/manufacturers/
-| Get a manufacturer -----| GET ---> http://localhost:8100/api/manufacturers/id/
-| Update a manufacturer --| PUT ---> http://localhost:8100/api/manufacturers/id/
-| Delete a manufacturer --| DELETE > http://localhost:8100/api/manufacturers/id/

-| List vehicle models ----| GET ---> http://localhost:8100/api/models/
-| Create a vehicle model -| POST --> http://localhost:8100/api/models/
-| Get a vehicle model ----| GET ---> http://localhost:8100/api/models/id/
-| Update a vehicle model -| PUT ---> http://localhost:8100/api/models/id/
-| Delete a vehicle model -| DELETE > http://localhost:8100/api/models/id/

-| List automobiles -------| GET ---> http://localhost:8100/api/automobiles/
-| Create an automobile ---| POST --> http://localhost:8100/api/automobiles/
-| Get a automobile -------| GET ---> http://localhost:8100/api/automobiles/vin/
-| Update an automobile ---| PUT ---> http://localhost:8100/api/automobiles/vin/
-| Delete an automobile ---| DELETE > http://localhost:8100/api/automobiles/vin/

​Sales - Port:8090
-| List salespeople -------| GET ---> http://localhost:8090/api/salespeople/
-| Create a salesperson ---| POST --> http://localhost:8090/api/salespeople/
-| Delete a salesperson ---| DELETE > http://localhost:8090/api/salespeople/id/

-| List customers ---------| GET ---> http://localhost:8090/api/customers/
-| Create a customer ------| POST --> http://localhost:8090/api/customers/
-| Delete a customer ------| DELETE > http://localhost:8090/api/customers/id/

-| List sales -------------| GET ---> http://localhost:8090/api/sales/
-| Create a sale ----------| POST --> http://localhost:8090/api/sales/
-| Delete a sale ----------| DELETE > http://localhost:8090/api/sales/id/

​Services - Port:8080
-| List technicians	---------------------| GET	--->http://localhost:8080/api/technicians/
-| Create a technician ------------------| POST	--->http://localhost:8080/api/technicians/
-| Delete a specific technician	---------| DELETE ->http://localhost:8080/api/technicians/:id/

-| List appointments --------------------| GET	--->http://localhost:8080/api/appointments/
-| Create an appointment ----------------| POST	--->http://localhost:8080/api/appointments/
-| Delete an appointment ----------------| DELETE ->http://localhost:8080/api/appointments/:id/
-| Set appointment status to "canceled"	-| PUT	--->http://localhost:8080/api/appointments/:id/cancel/
-| Set appointment status to "finished"	-| PUT	--->http://localhost:8080/api/appointments/:id/finish/

## Inventory API Insomnia Required Fields
 - Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.
​
## Sales API Insomnia Required Fields
Salesperson - Use JSON body to create:
    Send - {
        "first_name": "John",
        "last_name": "Doe",
        "employee_id": "JDoe##",
    },
    return - {
        "first_name": "John",
        "last_name": "Doe",
        "employee_id": "JDoe##",
    }

Customer - Use JSON body to create:
    Send - {
		"first_name": "Jane",
		"last_name": "Doe",
		"address": "An Example Address, City, State, ZIP",
		"phone_number": "###-###-####"
	},
    return - {
		"first_name": "Jane",
		"last_name": "Doe",
		"address": "An Example Address, City, State, ZIP",
		"phone_number": "###-###-####"
	}

Sale - Use JSON body to create: (does not change automobile.sold when used in Insomnia)
    Send - {
		"price": "#####.##",
		"automobile": "########", (automobile VIN)
		"salesperson": "JDoe##", (employee_id)
		"customer": "###-###-####" (phone_number)
	},
    return - {
        "pk": #,
		"price": #####.##,
		"automobile": {
            "vin": "########,
            "sold": true or false (does not change when used in Insomnia)
        },
		"salesperson": {
            "first_name": "John",
            "last_name": "Doe",
            "employee_id": "JDoe##"
        },
		"customer": {
            "first_name": "Jane",
		    "last_name": "Doe"
        }
	},

## Service API Insomnia Required Fields
Technician - Use JSON body to create:
!!!!employee_id must be a number from 0 to 32767!!!!
    Send - {
        "first_name": "Jane",
        "last_name": "Doe",
        "employee_id": "12345",
    },
    return - {
        "id": 1,
        "first_name": "Jane",
        "last_name": "Doe",
        "employee_id": "12345",
    }

Appointment - Use JSON body to create or update:
!!!status should be created, cancelled, or finished!!!
    Send - {
    "date_time": "2023-12-31T12:00:00+00:00",
    "reason": "Oil Change",
    "status": "created",
	"vin": "0001",
	"customer": "Jakob",
	"technician": "1"
    }
    return - {
	"id": 1,
	"date_time": "2023-12-31T12:00:00+00:00",
	"reason": "Oil Change",
	"status": "created",
	"vin": "0001",
	"customer": "Jakob",
	"technician": {
		"id": 1,
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": 12345
	}
    }

## Value Objects
 - The AutomobileVO is the value object in both the Sales and Services microservices that gets the automobile vins and sold state using a poller to update automatically.
