from django.urls import path
from .views import api_technicians, api_technician, api_appointments, api_appointment, api_cancel_appointment, api_finish_appointment



urlpatterns = [
    path('technicians/<int:pk>/', api_technician, name='api-technician'),
    path('technicians/', api_technicians, name='api-technicians'),
    path('appointments/<int:pk>/', api_appointment, name='api-appointment'),
    path('appointments/<int:pk>/cancel/', api_cancel_appointment, name='api-cancel-appointment'),
    path('appointments/<int:pk>/finish/', api_finish_appointment, name='api-finish-appointment'),
    path('appointments/', api_appointments, name='api-appointments'),
]
