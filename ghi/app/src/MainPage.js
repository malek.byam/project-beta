import './index.css';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        </div>
      <div className="image-container">
        <div className="image-single">
          <a href="/automobiles">
            <img className="image-small" src="inventory.jpg" alt="Cars" loading="lazy"/>
          </a>
          <div className="overlay-text">Inventory</div>
        </div>
        <div className="image-single">
          <a href="/sales">
            <img className="image-small" src="sales.jpg" alt="Sales" loading="lazy"/>
          </a>
          <div className="overlay-text">Sales</div>
        </div>
        <div className="image-single">
          <a href="/appointments">
            <img className="image-small" src="technician.jpg" alt="Service" loading="lazy"/>
          </a>
          <div className="overlay-text">Service</div>
        </div>
      </div>
      <footer className="footer">
        <p>©2023 CarCar. All rights reserved.</p>
      </footer>
    </div>
  );
}

export default MainPage;
