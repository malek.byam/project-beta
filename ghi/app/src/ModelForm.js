import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";

function ModelForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [picture, setPic] = useState('');
    const handlePicChange = (event) => {
        const value = event.target.value;
        setPic(value);
    }

    const [manufacturer, setManu] = useState('');
    const handleManuChange = (event) => {
        const value = event.target.value;
        setManu(value);
    }

    const [manufacturers, setManus] = useState([]);
    const getManusData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManus(data.manufacturers);
        }
    }

    useEffect(() => {
        getManusData();
    }, []);

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = picture
        data.manufacturer_id = manufacturer

        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setPic('');
            setManu('');
            navigate('/models');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Model</h1>
                    <form onSubmit={handleSubmit} id="create-model">
                        <div className="mb-1">
                            <select onChange={handleManuChange} val={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Select a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleNameChange} value={name} required placeholder="Model Name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handlePicChange} value={picture} placeholder="Model Picture" required type="url" name="picture" id="picture" className="form-control"/>
                            <label htmlFor="picture">Model Picture</label>
                        </div>
                        <button className="btn btn-primary">Add Model</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ModelForm
