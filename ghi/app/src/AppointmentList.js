import { useEffect, useState } from "react";

function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const getAppointmentsData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const appointmentsData = await response.json();
            setAppointments(appointmentsData.appointments)
        }
    }

    const [autos, setAutos] = useState([])
    const getAutosData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const autosData = await response.json();
            setAutos(autosData.autos)
        }
    }

    function ifVip(appointment) {
        for (let x of autos) {
            if (appointment.vin === x.vin) {
                return "Yes"
            }
        } return "No"
    };

    const cancelAppointment = async (import_id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${import_id}/cancel/`, {
            method: "PUT",
        });
        if (response.ok) {
            getAppointmentsData();
        }
    };

    const finishAppointment = async (import_id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${import_id}/finish/`, {
            method: "PUT",
        });
        if (response.ok) {
            getAppointmentsData();
        }
    };

    useEffect(() =>{
        getAppointmentsData()
        getAutosData()
    }, [])

    return (
        <div>
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">VIP</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Technician</th>
                        <th scope="col">Reason</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appointment => appointment.status === "created").map(appointment => {
                        const date = new Date(appointment.date_time);
                        const dateString = date.toDateString();
                        const timeString = date.toLocaleTimeString();
                        return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ ifVip(appointment) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ dateString}</td>
                            <td>{ timeString }</td>
                            <td>{ appointment.technician.first_name }</td>
                            <td>{ appointment.reason }</td>
                            <td><button
                                onClick={() => cancelAppointment(appointment.id)}
                                title="Cancel"
                                type="button"
                                >Cancel</button>
                                <button
                                onClick={() => finishAppointment(appointment.id)}
                                title="Finish"
                                type="button"
                                >Finish</button>
                            </td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentList
