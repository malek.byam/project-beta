import React, {useState} from "react";
import { useNavigate } from "react-router-dom";

function SalespersonForm() {
    const [firstName, setFName] = useState('');
    const handleFNameChange = (event) => {
        const value = event.target.value;
        setFName(value);
    }

    const [lastName, setLName] = useState('');
    const handleLNameChange = (event) => {
        const value = event.target.value;
        setLName(value);
    }

    const [employeeID, setEID] = useState('');
    const handleEIDChange = (event) => {
        const value = event.target.value;
        setEID(value);
    }

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const spUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(spUrl, fetchConfig);
        if (response.ok) {
            setFName('');
            setLName('');
            setEID('');
            navigate('/salespeople');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson">
                        <div className="form-floating mb-1">
                            <input onChange={handleFNameChange} value={firstName} required placeholder="First Name" type="text" id="firstname" name="firstname" className="form-control" />
                            <label htmlFor="firstname">First Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleLNameChange} value={lastName} required placeholder="Last Name" type="text" id="lastname" name="lastname" className="form-control" />
                            <label htmlFor="lastname">Last Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleEIDChange} value={employeeID} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Add Salesperson</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm
