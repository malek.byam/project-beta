import { useEffect, useState } from "react";

function SalespeopleList() {
    const [salespeople, setSP] = useState([])
    const getSPData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const spData = await response.json();
            setSP(spData.salespeople)
        }
    }

    useEffect(() =>{
        getSPData()
    }, [])

    return (
        <div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Employee ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                        <tr key={salesperson.employee_id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespeopleList
