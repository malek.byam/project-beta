import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";

function AutoForm() {
    const [vin, setVIN] = useState('');
    const handleVINChange = (event) => {
        const value = event.target.value;
        setVIN(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [year, setYear] = useState('');
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const [model, setModel] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const [models, setModels] = useState([]);
    const getModelData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        getModelData();
    }, []);

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.color = color
        data.year = year
        data.model_id = model

        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            setVIN('');
            setColor('');
            setYear('');
            setModel('');
            navigate('/automobiles');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile">
                        <div className="mb-1">
                            <select onChange={handleModelChange} value={model} required name="model" id="model" className="form-select">
                                <option value="">Select a Model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleVINChange} value={vin} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleColorChange} value={color} required placeholder="Color" type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleYearChange} value={year} required placeholder="Year" type="text" name="year" id="year" className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <button className="btn btn-primary">Add Automobile</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AutoForm
