import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";

function TechnicianForm() {
    const [first_name, setFirst_name] = useState('');
    const handleFirst_nameChange = (event) => {
        const value = event.target.value;
        setFirst_name(value);
    }

    const [last_name, setLast_name] = useState('');
    const handleLast_nameChange = (event) => {
        const value = event.target.value;
        setLast_name(value);
    }

    const [employee_id, setEmployee_id] = useState('');
    const handleEmployee_idChange = (event) => {
        const value = event.target.value;
        setEmployee_id(value);
    }

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id

        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            navigate('/technicians');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Tech</h1>
                    <form onSubmit={handleSubmit} id="create-technician">
                        <div className="form-floating mb-1">
                            <input onChange={handleFirst_nameChange} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control"/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleLast_nameChange} placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control"/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleEmployee_idChange} placeholder="Employee ID" required type="text" name="employee id" id="employee id" className="form-control"/>
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Add Technician</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm
