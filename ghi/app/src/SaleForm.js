import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";

function SaleForm() {
    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const [customer, setCust] = useState('');
    const handleCustChange = (event) => {
        const value = event.target.value;
        setCust(value);
    }

    const [customers, setCusts] = useState([]);
    const getCustData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const custData = await response.json();
            setCusts(custData.customers);
        }
    }

    const [salesperson, setSP] = useState('');
    const handleSPChange = (event) => {
        const value = event.target.value;
        setSP(value);
    }

    const [salespeople, setSPs] = useState([]);
    const getSPData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const spData = await response.json();
            setSPs(spData.salespeople);
        }
    }

    const [automobile, setAuto] = useState('');
    const handleAutoChange = (event) => {
        const value = event.target.value;
        setAuto(value);
    }

    const [autos, setAutos] = useState([]);
    const getAutoData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const autoData = await response.json();
            setAutos(autoData.autos);
        }
    }
    const filterAutos = autos.filter((auto) => auto.sold === false)

    useEffect(() => {
        getAutoData();
        getSPData();
        getCustData();
    }, []);

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = automobile;
        console.log(data);

        const url = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        console.log(fetchConfig);
        if (response.ok) {
            console.log(automobile)
            const aData = {};
            aData.sold = true;
            const autoUrl = `http://localhost:8100/api/automobiles/${automobile}/`
            const fetchAConfig = {
                method: "put",
                body: JSON.stringify(aData),
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            const response = await fetch(autoUrl, fetchAConfig);
            if (response.ok) {
                setAuto('');
                setSP('');
                setCust('');
                setPrice('');
                navigate('/sales');
            }
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale">
                        <div className="mb-1">
                            <select onChange={handleAutoChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Select an Automobile</option>
                                {filterAutos.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.model.name} {automobile.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-1">
                            <select onChange={handleSPChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Select a Salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-1">
                            <select onChange={handleCustChange} value={customer} required name="customer" id="customer" className="form-select">
                                <option value="">Select a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.phone_number} value={customer.phone_number}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handlePriceChange} value={price} required placeholder="Price" type="text" id="price" name="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Add Sale</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SaleForm
