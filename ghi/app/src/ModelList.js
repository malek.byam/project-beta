import { useEffect, useState } from "react";

function ModelList() {
    const [models, setModels] = useState([])
    const getModelData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const modelData = await response.json();
            setModels(modelData.models)
        }
    }

    useEffect(() =>{
        getModelData()
    }, [])

    return (
        <div>
            <h1>Models</h1>
            <table className="table table-striped position-relative">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img
                                    style={{width: "30%"}}
                                    src={model.picture_url}
                                    alt='carModel'
                                />
                            </td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ModelList
