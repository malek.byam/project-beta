import React, {useState} from "react";
import { useNavigate } from "react-router-dom";

function CustomerForm() {
    const [firstName, setFName] = useState('');
    const handleFNameChange = (event) => {
        const value = event.target.value;
        setFName(value);
    }

    const [lastName, setLName] = useState('');
    const handleLNameChange = (event) => {
        const value = event.target.value;
        setLName(value);
    }

    const [phoneNumber, setPN] = useState('');
    const handlePNChange = (event) => {
        const value = event.target.value;
        setPN(value);
    }

    const [address, setAddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.phone_number = phoneNumber;
        data.address = address;

        const custUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(custUrl, fetchConfig);
        if (response.ok) {
            setFName('');
            setLName('');
            setPN('');
            setAddress('');
            navigate('/customers');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer">
                        <div className="form-floating mb-1">
                            <input onChange={handleFNameChange} value={firstName} required placeholder="First Name" type="text" id="firstname" name="firstname" className="form-control" />
                            <label htmlFor="firstname">First Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleLNameChange} value={lastName} required placeholder="Last Name" type="text" id="lastname" name="lastname" className="form-control" />
                            <label htmlFor="lastname">Last Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handlePNChange} value={phoneNumber} required placeholder="Phone Number" type="text" id="phone_number" name="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number(###-###-####)</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleAddressChange} value={address} required placeholder="Address" type="text" id="address" name="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <button className="btn btn-primary">Add Customer</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm
