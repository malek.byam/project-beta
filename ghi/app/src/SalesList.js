import { useEffect, useState } from "react";

function SalesList() {
    const [sales, setSales] = useState([])
    const getSaleData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const saleData = await response.json();
            setSales(saleData.sales)
        }
    }

    useEffect(() =>{
        getSaleData()
    }, [])

    return (
        <div>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Salesperson Employee ID</th>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                        <tr key={sale.pk}>
                            <td>{ sale.salesperson.employee_id }</td>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalesList
