import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/vehicles" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicles
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby='navbarDropdown'>
                <li><a className="dropdown-item" href='/manufacturers'>Manufacturers</a></li>
                <li><a className="dropdown-item" href='/manufacturers/new'>Add Manufacturer</a></li>
                <li><a className="dropdown-item" href='/models'>Models</a></li>
                <li><a className="dropdown-item" href='/models/new'>Add Model</a></li>
                <li><a className="dropdown-item" href='/automobiles'>Automobiles</a></li>
                <li><a className="dropdown-item" href='/automobiles/new'>Add Automobile</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/sales" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby='navbarDropdown'>
                <li><a className="dropdown-item" href='/salespeople'>Salespeople</a></li>
                <li><a className="dropdown-item" href='/salespeople/new'>Add Salesperson</a></li>
                <li><a className="dropdown-item" href='/customers'>Customers</a></li>
                <li><a className="dropdown-item" href='/customers/new'>Add Customer</a></li>
                <li><a className="dropdown-item" href='/sales'>Sales</a></li>
                <li><a className="dropdown-item" href='/sales/new'>Add Sale</a></li>
                <li><a className="dropdown-item" href='/saleshistory'>Salesperson History</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/services" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby='navbarDropdown'>
                <li><a className="dropdown-item" href='/technicians'>Technicians</a></li>
                <li><a className="dropdown-item" href='/technicians/new'>Add Technician</a></li>
                <li><a className="dropdown-item" href='/appointments'>Appointments</a></li>
                <li><a className="dropdown-item" href='/appointments/new'>Add Appointment</a></li>
                <li><a className="dropdown-item" href='/appointments/history'>Service History</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
